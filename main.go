package main

import (
    "fmt"
    mqtt "github.com/eclipse/paho.mqtt.golang"
    "github.com/golang/protobuf/proto"
    "gitlab.com/zhangli2946/service/rtdproto"
    "sync/atomic"
    "time"
)

var (
    topicMeta        = "/89bcd9e0-eafe-11eb-b9d9-97a8a4f9df4f/for_histroy/for_histroy/metatag/retain"
    topicValue       = "/89bcd9e0-eafe-11eb-b9d9-97a8a4f9df4f/for_histroy/for_histroy/cachevalue/report"
    i          int64 = 0
)

func main() {
    opt := mqtt.NewClientOptions()
    opt.AddBroker("192.168.12.31:32566")
    client := mqtt.NewClient(opt)
    timer := time.NewTimer(time.Second)
    var payload []byte
    var err error
    if payload, err = proto.Marshal(
        &rtdproto.MetaTagSequence{
            Tags: []*rtdproto.MetaTag{
                {
                    Name: "histroy",
                    Type: rtdproto.ValueType_Integer,
                },
            },
        }); err != nil {
        fmt.Print(err)
        return
    }
    if tk := client.Connect(); tk.Wait() && tk.Error() != nil {
        fmt.Print(tk.Error().Error())
        return
    }

    if tk := client.Publish(topicMeta, 0, false, payload); tk.Wait() && tk.Error() != nil {
        fmt.Print(tk.Error().Error())
        return
    }
    for then := range timer.C {
        stamp := then.AddDate(0, 0, -1).UnixNano() / 1000000
        if payload, err = proto.Marshal(
            &rtdproto.ValueSequence{
                Values: []*rtdproto.NamedValue{
                    {
                        Name: "histroy",
                        Value: &rtdproto.RtdValue{
                            TimeStamp: stamp,
                            Value:     &rtdproto.RtdValue_IntVal{IntVal: atomic.AddInt64(&i, 1)},
                        },
                    },
                },
            }); err != nil {
            return
        }
        if tk := client.Publish(topicValue, 0, false, payload); tk.Wait() && tk.Error() != nil {
            return
        }
        timer.Reset(time.Second)
    }
}
